//
//  Profile.swift
//  TheBuild
//
//  Created by Collin Gardner on 8/25/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import UIKit

class Profile {
    
    //MARK: Properties
    
    var firstName: String
    var lastName: String
    var profilePhoto1: UIImage?
    var profilePhoto2: UIImage?
    var profilePhoto3: UIImage?
    var companyName: String
    var city: String
 
    
    //MARK: Initialization
    
    init?(firstName: String, lastName: String,
         profilePhoto1: UIImage?, profilePhoto2: UIImage?, profilePhoto3: UIImage?,
         companyName: String, city: String ) {
        
        
        
       /* if(firstName.isEmpty || lastName.isEmpty || companyName.isEmpty || city.isEmpty) {
            return nil
        } */
        
        guard !firstName.isEmpty else {
            return nil
        }
        
        guard !lastName.isEmpty else {
            return nil
        }
        guard !companyName.isEmpty else {
            return nil
        }
        
        guard !city.isEmpty else {
            return nil
        }
        
        
        // Initialize stored properties.
        self.firstName = firstName
        self.lastName = lastName
        self.profilePhoto1 = profilePhoto1
        self.profilePhoto2 = profilePhoto2
        self.profilePhoto3 = profilePhoto3
        self.companyName = companyName
        self.city = city
        
        
        
    }
}
