//
//  WelcomeScreenViewController.swift
//  TheBuild
//
//  Created by Collin Gardner on 8/26/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import UIKit
import Firebase

class WelcomeScreenViewController: UIViewController {

    
    
    var username:String = ""
    
  
    var ref:DatabaseReference = Database.database().reference()
 
    @IBOutlet weak var userNameLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameLabel?.text = "Welcome " + username
        //Setting Up Data
        let email = ref.child("users").child("user")
        print(email)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
