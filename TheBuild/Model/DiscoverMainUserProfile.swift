//
//  DiscoverMainProfile.swift
//  TheBuild
//
//  Created by Collin Gardner on 10/21/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import Foundation
struct DiscoverMainUserProfile {
    
    let firstName: String
    let lastName: String
    let company: String
    let city: String
    let position: String
    var likedUsers: Array<String>
    var unLikedUsers: Array<String>
    let userEmail: String
    
    var dictionary: [String: Any] {
        return [
            "firstName": firstName,
            "lastName": lastName,
            "company": company,
            "city": city,
            "position": position,
            "likedUsers": likedUsers,
            "unLikedUsers": unLikedUsers,
            "userEmail": userEmail
        ]
    }
    
}
extension DiscoverMainUserProfile{
    
    init?(dictionary: [String : Any], id: String) {
        guard let firstName = dictionary["firstName"] as? String,
            let lastName = dictionary["lastName"] as? String,
            let city = dictionary["cityName"] as? String,
            let company = dictionary["companyName"] as? String,
            let position = dictionary["jobPosition"] as? String,
            let likedUsers = dictionary["likedUsers"] as? Array<String>,
            let unLikedUsers = dictionary["unLikedUsers"] as? Array<String>,
            let userEmail = dictionary["userEmail"] as? String
            else { return nil }
        self.init(firstName: firstName, lastName: lastName, company: company, city: city, position: position,
                  likedUsers: likedUsers, unLikedUsers: unLikedUsers, userEmail: userEmail)
    }}
