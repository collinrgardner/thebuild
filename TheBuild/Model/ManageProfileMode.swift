//
//  ManageProfileMode.swift
//  TheBuild
//
//  Created by Collin Gardner on 10/6/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import Foundation


struct ManageProfileMode {
    
    let firstName: String
    let lastName: String
    let company: String
    let city: String
    let position: String
    let discoverUsersKey: String
    
    var dictionary: [String: Any] {
        print("Creating ManageProfileMode")
        return [
            "firstName": firstName,
            "lastName": lastName,
            "company": company,
            "city": city,
            "position": position,
            "discoverUsersKey": discoverUsersKey]
    }
    
}
extension ManageProfileMode{
    init?(dictionary: [String : Any], id: String) {
        guard let firstName = dictionary["firstName"] as? String,
            let lastName = dictionary["lastName"] as? String,
            let city = dictionary["cityName"] as? String,
            let company = dictionary["companyName"] as? String,
            let position = dictionary["jobPosition"] as? String,
            let discoverUsersKey = dictionary["discoverUsersKey"] as? String
            else { return nil }
        self.init(firstName: firstName, lastName: lastName, company: company, city: city, position: position, discoverUsersKey: discoverUsersKey)
    }}
