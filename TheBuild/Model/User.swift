//
//  User.swift
//  TheBuild
//
//  Created by Collin Gardner on 9/22/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import Foundation
import Firebase

struct User {
    
    let uid: String
    let email: String
    
    init(authData: Firebase.User) {
        uid = authData.uid
        email = authData.email!
    }
    
    init(uid: String, email: String) {
        self.uid = uid
        self.email = email
    }
}
