//
//  FirstViewController.swift
//  TheBuild
//
//  Created by Collin Gardner on 8/14/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import UIKit
import Firebase


//This class will load all the chats with different users, and last messages sent, and theneach cell can be clicked into a new view controller, pass the chat name, and that will load the messages of the specific chats, this the navigator like iMessages, and the other is the actual chat controller
//This will take in a list of the users chats (chatKeys) from the datbase, and display each chat in a cell. it then has the ability to refresh each chat.  A chatKey will be the other users uniqueID?


class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,    UITextFieldDelegate {

    

    @IBOutlet weak var clientTable: UITableView!
    
    //From Firebase tutorial
    // Instance variables
     @IBOutlet weak var textField: UITextField!
     @IBOutlet weak var sendButton: UIButton!
     var ref: DatabaseReference!
     var messages: [DocumentSnapshot]! = []
     var msglength: NSNumber = 10
    
  //   var storageRef: StorageReference!
 //    var remoteConfig: RemoteConfig!

    
      var db:Firestore = Firestore.firestore()
      var user: User!
      fileprivate var _refHandle: DatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    user = User(uid: "FakeId", email: "hungry@person.food")
        Auth.auth().addStateDidChangeListener { auth, user in
            guard let user = user else { return }
            self.user = User(authData: user)
        }
        self.clientTable.register(UITableViewCell.self, forCellReuseIdentifier: "tableViewCell")
        configureDatabase()
    }
//    deinit {
//       if let refHandle = _refHandle  {
//        // self.ref.child("messages").removeObserver(withHandle: refHandle)
//       }
//     }

    

    func configureDatabase() {
      // Listen for new messages in the Firebase database
        //Todo change This below to the messages between 2 users
        db.collection("messages").document("testMessage")
        .addSnapshotListener { [weak self] documentSnapshot, error in
          guard let document = documentSnapshot else {
            print("Error fetching document: \(error!)")
            return
          }
          guard let data = document.data() else {
            print("Document data was empty.")
            return
          }
          print("Current data: \(data)")
            guard let strongSelf = self else { return }
            //todo before this check if document snapshot is not null
            //print(clientTable)
            strongSelf.messages.append(documentSnapshot!)
            print("added")
            print(strongSelf.messages)
            print(strongSelf.clientTable)
            print(strongSelf.clientTable.dataSource)
            self?.clientTable.dataSource = self//or this could be strong self
            strongSelf.clientTable.insertRows(at: [IndexPath(row: strongSelf.messages.count-1, section: 0)], with: .automatic)
            print("db configured done")
        }

//        _refHandle = self.db.collection("messages").document().onWr
//        _refHandle = self.db.collection("messages").observe(NSNull, with: { [weak self] (snapshot) -> Void in
//        guard let strongSelf = self else { return }
//        strongSelf.messages.append(snapshot)
//        strongSelf.clientTable.insertRows(at: [IndexPath(row: strongSelf.messages.count-1, section: 0)], with: .automatic)
//      })
    }

    @IBAction func OpenManageProfilePage(_ sender: Any) {
        print("button pressed")
    }
    
    @IBAction func signOutDidTouch(_ sender: Any) {
        
        print("User is", user.email)
        // 1 You first get the currentUser and create onlineRef using its uid, which is a unique identifier representing the user
        let user = Auth.auth().currentUser!
        let onlineRef = Database.database().reference(withPath: "online/\(user.uid)")
        
        
        // 2 You call removeValue to delete the value for onlineRef. While Firebase automatically adds the user to online upon sign in, it does not remove the user on sign out. Instead, it only removes users when they become disconnected. For this application, it doesn’t make sense to show users as “online” after they log out, so you manually remove them here.
        print("User is + ", user.email!)
        print(onlineRef.key)

    
        onlineRef.removeValue() { (error, _) in
            // 3 Within the completion closure, you first check if there’s an error and simply print it if so.
            if let error = error {
                print("Removing online failed: \(error)")
              //  return//Permission Error, but still signs the user out
                // Follow this link here to fix issue if need be: // https://stackoverflow.com/questions/37403747/firebase-permission-denied
           }
            
            // 4 You here call Auth.auth().signOut() to remove the user’s credentials from the keychain. If there isn’t an error, you dismiss the view controller. Otherwise, you print out the error.
            do {
                try Auth.auth().signOut()
                self.dismiss(animated: true, completion: nil)
            } catch (let error) {
                print("Auth sign out failed: \(error)")
            }
        
        
    }
    }
    
    @IBAction func didSendMessage(_ sender: UIButton) {
       _ = textFieldShouldReturn(textField)
     }
    
    
    // UITextViewDelegate protocol methods
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       guard let text = textField.text else { return true }
       textField.text = ""
       view.endEditing(true)
        let data = ["text": text]
       sendMessage(withData: data)
       return true
     }

     func sendMessage(withData data: [String: String]) {
       var mdata = data
       mdata["name"] = Auth.auth().currentUser?.displayName
       if let photoURL = Auth.auth().currentUser?.photoURL {
        // mdata[Constants.MessageFields.photoURL] = photoURL.absoluteString
       }
        //Todo change this to update the firestore
        self.db.collection("messages").document("testMessage").setData(mdata)
       // Push data to Firebase Database
       //self.ref.child("messages").childByAutoId().setValue(mdata)
     }

    
    // UITableViewDataSource protocol methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return messages.count
    }

    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("setting up table view")
       // Dequeue cell
   let cell = self.clientTable.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath)
       // Unpack message from Firebase DataSnapshot
       let messageSnapshot = self.messages[indexPath.row]
        guard let message = messageSnapshot.data() as? [String: String] else { return cell }
       let name = message["name"] ?? ""
       let text = message["text"] ?? ""
       cell.textLabel?.text = name + ": " + text
       cell.imageView?.image = UIImage(named: "ic_account_circle")
       if let photoURL = message["photoUrl"], let URL = URL(string: photoURL),
           let data = try? Data(contentsOf: URL) {
        // cell.imageView?.image = UIImage(data: data)
       }
       return cell
     }
    
    
    }

