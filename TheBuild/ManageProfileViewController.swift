//
//  ManageProfileViewController.swift
//  TheBuild
//
//  Created by Collin Gardner on 8/25/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import UIKit
import Firebase

class ManageProfileViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var positionTextField: UITextField!

    @IBOutlet weak var emailLabelField: UILabel!
     var ref:DatabaseReference = Database.database().reference()
    var db:Firestore = Firestore.firestore()
////    var userID: String? //might be
//    var profile: ManageProfileMode
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //You might want to use a guard, as a force unwrap might break your app if a user isn’t signed in.
        guard let user = Auth.auth().currentUser else { return }
        guard let userID = Auth.auth().currentUser?.uid else { return }
        print(userID)
        let docRef =  db.collection("users").document(userID)
        print(docRef)
         docRef.getDocument { (document, error) in
            if let profile = document.flatMap({
                $0.data().flatMap({ (data) in
                    return ManageProfileMode(dictionary: data, id: userID)
                })
            }) {
                self.firstNameTextField.text = profile.firstName
                self.lastNameTextField.text = profile.lastName
                self.companyTextField.text = profile.company
                self.positionTextField.text = profile.position
                self.cityTextField.text = profile.city
            } else {
                print("Document does not exist")
                print(error)
            }
        }
        // we could display user info after this, like it can be prefilled using the same logic from below
            emailLabelField.text = user.email;
        print(userID)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveDidTouch(_ sender: Any) {
       // let firstName = firstNameTextField.text;
        guard let userID = Auth.auth().currentUser?.uid else { return }
        guard let email = Auth.auth().currentUser?.email else { return } //Todo return error, or retry
        // Add a new document in collection "users"
        
        var documentName = cleanCompanyName() + "_" + cleanCityName();
        
        
        db.collection("users").document(userID).setData([
            "firstName": firstNameTextField.text!,
            "lastName": lastNameTextField.text!,
            "companyName": companyTextField.text!,
            "cityName": cityTextField.text!,
            "jobPosition":positionTextField.text!,
            "discoverUsersKey": documentName.lowercased()
        ]) { err in
            if let err = err {
                print("Error writing document to Auth Table: \(err)")
            } else {
                print("Document successfully written! in users")
            }
        }
        
        //add new user to discover users tables
        db.collection("discover_users").document(documentName.lowercased()).collection("users").document(email).setData([
            "firstName": firstNameTextField.text!,
            "lastName": lastNameTextField.text!,
            "companyName": companyTextField.text!,
            "cityName": cityTextField.text!,
            "jobPosition":positionTextField.text!,
            "likedUsers":[String](),
            "unLikedUsers":[String](),
            "userEmail": email
        ]) { err in
            if let err = err {
                print("Error writing document to Auth Table: \(err)")
            } else {
                print("Document successfully written! in discover_users")
            }
        }
        


        //This is for making test Data
//        for n in 1...10 {
//
//            db.collection("discover_users").document("sherwinwilliams_cleveland").collection("users").document("testguy"+String(n)+"@mail.com").setData([
//                "firstName": "testFirstName" + String(n),
//                "lastName": "testLastName" + String(n),
//                "companyName": "testCompanyName" + String(n),
//                "cityName": "testCityName" + String(n),
//                "jobPosition": "testJobPosition" + String(n),
//                "likedUsers": [String](),
//                "unLikedUsers": [String](),
//                "userEmail": "testGuy"+String(n)+"@mail.com"
//            ]) { err in
//                if let err = err {
//                    print("Error writing document to Auth Table: \(err)")
//                } else {
//                    print("Document successfully written!")
//                }
//            }
//        }//End of for loop


        
    }
    
    func cleanCompanyName() -> String {
        if(!companyTextField.text!.isEmpty) {
            return companyTextField.text!
        } else {
            //Todo Change This to throw and Error
            return ""
        }
    }//end cleanCompanyName
    
    
    func cleanCityName() -> String {
        if(!cityTextField.text!.isEmpty) {
            return cityTextField.text!
        } else {
            //Todo Change This to throw and Error
            return ""
        }
    } //end of cleanCityName
}
