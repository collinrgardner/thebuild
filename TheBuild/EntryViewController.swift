//
//  EntryViewController.swift
//  TheBuild
//
//  Created by Collin Gardner on 9/15/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import UIKit
import Firebase

class EntryViewController: UIViewController {
    
    let loginIntoMain =  "LoginIntoMain"


    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Create an authentication observer using addStateDidChangeListener(_:). The block is passed two parameters: auth and user
        Auth.auth().addStateDidChangeListener() { auth, user in
            // Test the value of user. Upon successful user authentication, user is populated with the user’s information. If authentication fails, the variable is nil.
            if user != nil {
                
                print("User is not Null")
                // On successful authentication, perform the segue and clear the text fields’ text. It may seem strange that you don’t pass the user to the next controller, but you’ll see how to get this within GroceryListTableViewController.swift
                self.performSegue(withIdentifier: self.loginIntoMain, sender: nil)
                self.emailTextField.text = nil
                self.passwordTextField.text = nil
            }
        }
        
    
        // Do any additional setup after loading the view.
    }
    @IBAction func loginDidTouch(_ sender: AnyObject) {
        guard
            let email = emailTextField.text,
            let password = passwordTextField.text,
            email.count > 0,
            password.count > 0
            else {
                return
        }
        
        print("email is ", email)
        print("password is", password)
        
        Auth.auth().signIn(withEmail: email, password: password) { user, error in
            if let error = error, user == nil {
                let alert = UIAlertController(title: "Sign In Failed",
                                              message: error.localizedDescription,
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                
                self.present(alert, animated: true, completion: nil)
                print("User Logged in")
                //Need to sign the user out now
            }
        }
        
    }
    
    
    
    @IBAction func signUpDidTouch(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Register",
                                      message: "Register",
                                      preferredStyle: .alert)
        print("touched Sign Up")
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            
            // Get Email and password Get the email and password as supplied by the user from the alert controller. emailField = alert.textFields![0]
            let emailField = alert.textFields![0]//Different than the above from the text fields, supposed to be this way
            let passwordField = alert.textFields![1]
            print("now here")
            // Call createUser(withEmail:password:) on the default Firebase auth object passing the email adn password.
            Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) { user, error in
                if error == nil {
                    print("lastly")
                    // If there are no errors, the user account has been created. However, you still need to authenticate this new user, so call signIn(withEmail:password:), again passing in the supplied email and password.
                    Auth.auth().signIn(withEmail: self.emailTextField.text!,
                                       password: self.passwordTextField.text!)
                    print("Sent User in");
                } else {
                    print("error: ", error ?? "no error")//How can we get this error,
                    //todo User Needs Alerted of this Error
                }
            }
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        print("here")
        alert.addTextField { emailTextField in
            emailTextField.placeholder = "Enter your email"
        }
        print("this")
        alert.addTextField { passwordTextField in
            passwordTextField.isSecureTextEntry = true
            passwordTextField.placeholder = "Enter your password"
        }
            print("this")
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
            print("this")
        present(alert, animated: true, completion: nil)
        
        
        
    }
    
}
    

   




/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


