//
//  DiscoverViewController.swift
//  
//
//  Created by Collin Gardner on 10/8/19.
//

import UIKit
import Firebase

class DiscoverViewController: UIViewController {
    
    var db:Firestore = Firestore.firestore()
    var discoverMainProfile: DiscoverMainUserProfile?
    var matchesArray: Array = [DiscoverMainUserProfile]()
    var dislayedUserEmail: String = ""
    var discoverUsersKey: String = ""
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var companyTextField: UITextField!
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var jobTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let userID = Auth.auth().currentUser?.uid else { return }
        guard let userEmail = Auth.auth().currentUser?.email else { return }
        
        
        //Get users users from collection, show first 10 users, check and see that they are not in the users profile
        
        //This could also be an updated matching list that updates each time the user goes through it. it populates the user for 5 matches per round of going through. Meanwhile, we just populate the users potentialsMatches in the database, and when they go to this page each time it displayes that for them.
        
        //get List of 5 users that the user can match with, and add to global variable
        
        //Get Users discoverUserKey from object from Users Collection
        //
        //        let group = DispatchGroup()
        //        group.enter()
        //
        //
        self.getDiscoverUsersKey(userId:userID, userEmail:userEmail)
        
        
        
        
        
        
        
        //delete next 21 lines, i believe they are old code
        //        let people = docRef.data()!["likedUsers"]! as [Any]
        //        print(people[0])
        
        //        docRef.getDocument { (document, error) in
        //            if let profile = document.flatMap({
        //                $0.data().flatMap({ (data) in
        //                    return ManageProfileMode(dictionary: data, id: userID)
        //                })
        //            }) {
        //                self.firstNameTextField.text = profile.firstName
        //                self.lastNameTextField.text = profile.lastName
        //                self.companyTextField.text = profile.company
        //                self.positionTextField.text = profile.position
        //                self.cityTextField.text = profile.city
        //            } else {
        //                print("Document does not exist")
        //                print(error)
        //            }
        //        }
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //First Function Called In Process
    func getDiscoverUsersKey(userId:String, userEmail:String) {
        let docRef =  db.collection("users").document(userId)
        docRef.getDocument {(document, error) in
            if let profile = document.flatMap({
                $0.data().flatMap({ (data) in
                    return ManageProfileMode(dictionary: data, id: userId)
                })
            }) {
                self.discoverUsersKey = profile.discoverUsersKey
                self.getCurrentUserFromDiscoverUsersTable(userEmail: userEmail, userID: userId, discoverUsersKey: profile.discoverUsersKey)
                
            } else {
                //Todo  show and error
                print("Document does not exist")
                print(error)
            }
        }
    }
    
    //Second Function Called In Process
    func getCurrentUserFromDiscoverUsersTable(userEmail: String, userID: String, discoverUsersKey: String) {
        print(discoverUsersKey)
        let discoverUsersDocRef  = db.collection("discover_users").document(discoverUsersKey).collection("users").document(userEmail)
        discoverUsersDocRef.getDocument {(document, error) in
            if let profile = document.flatMap({
                $0.data().flatMap({ (data) in
                    return DiscoverMainUserProfile(dictionary: data, id: userID)
                })
            }) {
                self.discoverMainProfile = profile;
                self.displayerUsers(discoverUsersKeyStr: discoverUsersKey)
            } else {
                print("Error retrieving Document here from discoverUsers")
                print(error)
            }
        }
    }
    
    //Third Function Called In process, called from viewDidLoad function series
    //This gets all the users in the same users table for city, company
    func displayerUsers(discoverUsersKeyStr: String) {
        //Now get the users profile from discover_users tabl
        print(discoverUsersKeyStr)
        db.collection("discover_users").document(discoverUsersKeyStr).collection("users").getDocuments {  (querySnapshot, err) in
            
            if let err = err
            {
                print("Error getting documents: \(err)");
            }
            else
            {
                for document in querySnapshot!.documents {
                    print("this")
                    print(document.data());
                    print(self.matchesArray)
                    print("lastName")
                    print(document.get("lastName"))
                    self.matchesArray.append(
                        DiscoverMainUserProfile(firstName: document.get("firstName") as? String ?? "",
                                                lastName: document.get("lastName") as? String ?? "",
                                                company: document.get("companyName") as? String ?? "",
                                                city: document.get("cityName") as? String ?? "",
                                                position: document.get("jobPosition") as? String ?? "",
                                                likedUsers: [String](),
                                                unLikedUsers: [String](),
                                                userEmail: document.get("userEmail") as? String ?? "")
                    )
                    print("end")
                    print("\(document.documentID) => \(document.data())");
                }
                
                self.cycleUsers();
                //Call cycle users here
            }
        }
        // Now we can take the users information from, and display matches for the other users.
        //We will display users from their collection they are in that do not share the same liked, and disliked users of
    }
    
    
    
    //This function is called any time a new user is whow
    func cycleUsers() {
        print(matchesArray)
        print(matchesArray.count)
        //        if(!matchesArray.isEmpty) {
        //
        //        }
        //
        var displayedUser: DiscoverMainUserProfile
        if let user = self.matchesArray.popLast() {
            displayedUser = user
            if(!(self.discoverMainProfile?.likedUsers.contains(displayedUser.userEmail))!
                && !self.discoverMainProfile!.unLikedUsers.contains(displayedUser.userEmail)) {
                firstNameTextField.text = displayedUser.firstName;
                lastNameTextField.text = displayedUser.lastName;
                cityTextField.text = displayedUser.city;
                jobTextField.text = displayedUser.position;
                companyTextField.text = displayedUser.company;
                self.dislayedUserEmail = displayedUser.userEmail;
            } else {
                cycleUsers()
            }
            
        }
        
        //        if( displayedUser != nil && displayedUser?.userEmail != nil) {
        //
        //
        //
        //
        //
        //o        //        }
        //if displayed user email is not the self.discoverMainProfile email, then display, else continue in the loop, this can be like  while loop
        
        
    }
    
    @IBAction func likedUser(_ sender: Any) {
        //Add User being shown to likedUser, and then just call call cycle Users
        if self.discoverMainProfile != nil {
            if self.discoverMainProfile?.likedUsers != nil {
                self.discoverMainProfile?.likedUsers.append(self.dislayedUserEmail)
                
            }
        }
        cycleUsers()
    }
    
    @IBAction func didNotLikedUser(_ sender: Any) {
        if self.discoverMainProfile != nil {
            if self.discoverMainProfile?.unLikedUsers != nil {
                self.discoverMainProfile?.unLikedUsers.append(self.dislayedUserEmail)
                
            }
        }
        cycleUsers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("writing user modified liked and unliked last back to the database")
        print(discoverMainProfile)
        db.collection("discover_users").document(self.discoverUsersKey).collection("users").document(self.discoverMainProfile?.userEmail ?? "").updateData(["likedUsers": self.discoverMainProfile?.likedUsers,
                                                                                                                                                            "unLikedUsers": self.discoverMainProfile?.unLikedUsers]){ err in
                                                                                                                                                                if let err = err {
                                                                                                                                                                    print("Error writing document to Auth Table: \(err)")
                                                                                                                                                                } else {
                                                                                                                                                                    print("Document successfully written!")
                                                                                                                                                                }
        }
    }
    
    
}
