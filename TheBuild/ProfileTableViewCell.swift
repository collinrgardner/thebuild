//
//  ProfileTableViewCell.swift
//  TheBuild
//
//  Created by Collin Gardner on 8/25/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {



    @IBOutlet weak var firstNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
