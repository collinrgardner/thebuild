//
//  TheBuildTests.swift
//  TheBuildTests
//
//  Created by Collin Gardner on 8/14/19.
//  Copyright © 2019 Collin Gardner. All rights reserved.
//

import XCTest
@testable import TheBuild

class TheBuildTests: XCTestCase {
    
      //MARK: Profile Class Tests

    // Confirm that the Profile initializer returns a Profile object when passed valid parameters.
    func testProfileInitializationSucceeds() {
        let fullProfileNoPhotos = Profile.init(firstName: "Collin", lastName: "gardner", profilePhoto1: nil,
                                       profilePhoto2: nil, profilePhoto3: nil, companyName: "company",
                                       city: "chardon")
        XCTAssertNotNil(fullProfileNoPhotos)
    }
    
    // Confirm that the Profile initialier returns nil when passed a negative rating or an empty name.
    func testProfileInitializationFails() {
        let profileNoFirst = Profile.init(firstName: "", lastName: "gardner", profilePhoto1: nil,
                                               profilePhoto2: nil, profilePhoto3: nil, companyName: "company",
                                               city: "chardon")
        XCTAssertNil(profileNoFirst)
        
        let profileNoLast = Profile.init(firstName: "Collin", lastName: "", profilePhoto1: nil,
                                               profilePhoto2: nil, profilePhoto3: nil, companyName: "company",
                                               city: "chardon")
        XCTAssertNil(profileNoLast)
        
        let profileNoCompany = Profile.init(firstName: "Collin", lastName: "gardner", profilePhoto1: nil,
                                               profilePhoto2: nil, profilePhoto3: nil, companyName: "",
                                               city: "chardon")
        XCTAssertNil(profileNoCompany)
        
        let profileNoCity = Profile.init(firstName: "Collin", lastName: "gardner", profilePhoto1: nil,
                                               profilePhoto2: nil, profilePhoto3: nil, companyName: "company",
                                               city: "")
        XCTAssertNil(profileNoCity)
    }
    
  
}
